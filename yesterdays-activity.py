#!/usr/bin/env python

from datetime import date, timedelta
from src.activity import Activity


if __name__ == "__main__":
    Activity().summarize(date.today() - timedelta(days=1))
