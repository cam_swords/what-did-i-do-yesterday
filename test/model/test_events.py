from io import StringIO
from unittest import TestCase

from src.model import Event, Events


class TestEvents(TestCase):

    def test_should_sort_before_printing(self):
        a = Event('a.message')
        b = Event('b.message')
        events = Events([b, a])

        writer = StringIO()
        events.summarize(writer.write)

        self.assertEqual('a.messageb.message', writer.getvalue())

    def test_should_remove_duplicate_events_when_printing(self):
        a1 = Event('a.message')
        a2 = Event('a.message')
        b = Event('b.message')
        events = Events([a1, a2, b])

        writer = StringIO()
        events.summarize(writer.write)

        self.assertEqual('a.messageb.message', writer.getvalue())
