from typing import Dict

from io import StringIO
from unittest import TestCase
from src.parser.parsers.pushed_to_new_branch_parser import PushedToNewBranchParser


class TestPushedToNewBranchParser(TestCase):

    def test_should_parse_push_to_new_branch(self):
        event = PushedToNewBranchParser().parse(self.push_to_branch())
        output = StringIO()

        event.summarize(output.write)
        self.assertEqual('Pushed 2 commit(s) to new branch rename-executed-rule, '
                         'latest commit Rename executed rules to rules', output.getvalue())

    def push_to_branch(self) -> Dict[str, str]:
        return {
            'id': 893455741,
            'project_id': 9762266,
            'action_name': 'pushed new',
            'target_id': None,
            'target_iid': None,
            'target_type': None,
            'author_id': 4312863,
            'target_title': None,
            'created_at': '2020-09-04T05:35:00.003Z',
            'author': {'id': 4312863,
                       'name': 'Jo Bloggs',
                       'username': 'jo_bloggs',
                       'state': 'active',
                       'avatar_url': 'https://secure.gravatar.com/avatar/1234?s=80&d=identicon',
                       'web_url': 'https://gitlab.com/jo_bloggs'},
            'push_data': {'commit_count': 2,
                          'action': 'created',
                          'ref_type': 'branch',
                          'commit_from': None,
                          'commit_to': '76e6e85cdde0d5c60dd0ebe6cc21c5c0e4968af2',
                          'ref': 'rename-executed-rule',
                          'commit_title': 'Rename executed rules to rules',
                          'ref_count': None},
            'author_username': 'jo_bloggs'
        }
