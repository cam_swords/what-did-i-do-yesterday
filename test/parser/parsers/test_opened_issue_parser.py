from typing import Dict

from io import StringIO
from unittest import TestCase
from src.parser.parsers.opened_issue_parser import OpenedIssueParser


class TestOpenedIssueParser(TestCase):

    def test_should_parse_new_merge_request(self):
        event = OpenedIssueParser().parse(self.new_opened_issue())
        output = StringIO()

        event.summarize(output.write)
        self.assertEqual('Created Issue #244834 Show the injection parameter', output.getvalue())

    def new_opened_issue(self) -> Dict[str, str]:
        return {
            'id': 891777877,
            'project_id': 278964,
            'action_name': 'opened',
            'target_id': 70663870,
            'target_iid': 244834,
            'target_type': 'Issue',
            'author_id': 4312863,
            'target_title': 'Show the injection parameter',
            'created_at': '2020-09-03T04:39:24.059Z',
            'author': {'id': 4312863,
                       'name': 'Jo Bloggs',
                       'username': 'jobloggs',
                       'state': 'active',
                       'avatar_url': 'https://secure.gravatar.com/avatar/123456?s=80&d=identicon',
                       'web_url': 'https://gitlab.com/jo_bloggs'},
            'author_username': 'jo_bloggs'
        }
