from typing import Dict

from io import StringIO
from unittest import TestCase
from src.parser.parsers.delete_branch_parser import DeleteBranchParser


class TestDeleteBranchParser(TestCase):

    def test_should_parse_delete_branch(self):
        event = DeleteBranchParser().parse(self.delete_branch())
        output = StringIO()

        event.summarize(output.write)
        self.assertEqual('Deleted branch add-integration-tests', output.getvalue())

    def delete_branch(self) -> Dict[str, str]:
        return {
            'id': 893491296,
            'project_id': 9762266,
            'action_name': 'deleted',
            'target_id': None,
            'target_iid': None,
            'target_type': None,
            'author_id': 4312863,
            'target_title': None,
            'created_at': '2020-09-04T06:21:21.198Z',
            'author': {'id': 4312863,
                       'name': 'Jo Bloggs',
                       'username': 'jo_bloggs',
                       'state': 'active',
                       'avatar_url': 'https://secure.gravatar.com/avatar/1234?s=80&d=identicon',
                       'web_url': 'https://gitlab.com/jo_bloggs'},
            'push_data': {'commit_count': 0,
                          'action': 'removed',
                          'ref_type': 'branch',
                          'commit_from': '6ec340410e6fb8feed0420d49e24ac8e450c4ad2',
                          'commit_to': None,
                          'ref': 'add-integration-tests',
                          'commit_title': None,
                          'ref_count': None},
            'author_username': 'jo_bloggs'
        }

