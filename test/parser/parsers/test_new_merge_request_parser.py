from typing import Dict

from io import StringIO
from unittest import TestCase
from unittest.mock import MagicMock
from src.parser.parsers.new_merge_request_parser import NewMergeRequestParser
from src.model.project import Project


class TestNewMergeRequestParser(TestCase):

    def test_should_parse_new_merge_request(self):
        project_service = MagicMock()
        project_service.load.return_value = Project('http://project')

        event = NewMergeRequestParser(project_service).parse(self.new_merge_request())
        output = StringIO()

        event.summarize(output.write)
        self.assertIn('Created MR Load API scripts from file http://project', output.getvalue())

    def new_merge_request(self) -> Dict[str, str]:
        return {
            'id': 884710474,
            'project_id': 9762266,
            'action_name': 'opened',
            'target_id': 69015218, 'target_iid': 278,
            'target_type': 'MergeRequest',
            'author_id': 4312863,
            'target_title': 'Load API scripts from file',
            'created_at': '2020-08-28T03:44:55.322Z',
            'author': {
                'id': 4312863,
                'name': 'Jo Bloggs',
                'username': 'jbloggs',
                'state': 'active',
                'avatar_url': 'https://secure.gravatar.com/avatar/12345?s=80&d=identicon',
                'web_url': 'https://gitlab.com/jbloggs'
            },
            'author_username': 'jbloggs'
        }
