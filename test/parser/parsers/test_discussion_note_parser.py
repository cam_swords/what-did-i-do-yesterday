from typing import Dict

from io import StringIO
from unittest import TestCase
from src.parser.parsers.discussion_note_parser import DiscussionNoteParser


class TestDiscussionNoteParser(TestCase):

    def test_should_parse_discussion_note(self):
        event = DiscussionNoteParser().parse(self.discussion_note())
        output = StringIO()

        event.summarize(output.write)
        self.assertEqual('Commented on Issue #215010 Show all DAST vulnerabilities on the Security Dashboard', output.getvalue())

    def discussion_note(self) -> Dict[str, str]:
        return {
            'id': 883033661, 'project_id': 278964, 'action_name': 'commented on', 'target_id': 402762947,
            'target_iid': 402762947, 'target_type': 'DiscussionNote', 'author_id': 4312863,
            'target_title': 'Show all DAST vulnerabilities on the Security Dashboard',
            'created_at': '2020-08-27T01:44:24.686Z', 'note': {'id': 402762947, 'type': 'DiscussionNote',
                                                               'body': "Fair point. Does this mean you're ok with Proposal 2?",
                                                               'attachment': None,
                                                               'author': {'id': 4312863, 'name': 'Jo Bloggs',
                                                                          'username': 'jo_bloggs', 'state': 'active',
                                                                          'avatar_url': 'https://secure.gravatar.com/avatar/???',
                                                                          'web_url': 'https://gitlab.com/jo_bloggs'},
                                                               'created_at': '2020-08-27T01:44:20.676Z',
                                                               'updated_at': '2020-08-27T01:44:20.676Z',
                                                               'system': False,
                                                               'noteable_id': 33505313, 'noteable_type': 'Issue',
                                                               'resolvable': False, 'confidential': False,
                                                               'noteable_iid': 215010, 'commands_changes': {}},
            'author': {'id': 4312863, 'name': 'Jo Bloggs', 'username': 'jo_bloggs', 'state': 'active',
                       'avatar_url': 'https://secure.gravatar.com/avatar/???',
                       'web_url': 'https://gitlab.com/jo_bloggs'}, 'author_username': 'jo_bloggs'
        }
