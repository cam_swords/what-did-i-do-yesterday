from typing import Dict

from io import StringIO
from unittest import TestCase
from unittest.mock import MagicMock
from src.parser.parsers.approved_merge_request_parser import ApprovedMergeRequestParser
from src.project import Project


class TestApprovedMergeRequestParser(TestCase):

    def test_should_parse_approved_merge_request(self):
        project_service = MagicMock()
        project_service.load.return_value = Project('http://project')

        event = ApprovedMergeRequestParser(project_service).parse(self.new_approved_merge_request())
        output = StringIO()

        event.summarize(output.write)
        self.assertEqual('Approved MR Add body to evidence in DAST schema http://project/-/merge_requests/37',
                         output.getvalue())

    def new_approved_merge_request(self) -> Dict[str, str]:
        return {
            'id': 891763006,
            'project_id': 16683102,
            'action_name': 'accepted',
            'target_id': 64838453,
            'target_iid': 37,
            'target_type': 'MergeRequest',
            'author_id': 4312863,
            'target_title': 'Add body to evidence in DAST schema',
            'created_at': '2020-09-03T04:18:45.168Z',
            'author': {'id': 4312863,
                       'name': 'Jo Bloggs',
                       'username': 'jo_bloggs',
                       'state': 'active',
                       'avatar_url': 'https://secure.gravatar.com/avatar/12345?s=80&d=identicon',
                       'web_url': 'https://gitlab.com/jo_bloggs'},
            'author_username': 'jo_bloggs'
        }
