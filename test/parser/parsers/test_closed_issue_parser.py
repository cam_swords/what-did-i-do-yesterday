from typing import Dict

from io import StringIO
from unittest import TestCase
from src.parser.parsers.closed_issue_parser import ClosedIssueParser


class TestClosedIssueParser(TestCase):

    def test_should_parse_new_merge_request(self):
        event = ClosedIssueParser().parse(self.new_opened_issue())
        output = StringIO()

        event.summarize(output.write)
        self.assertEqual('Closed Issue #230687 Investigate performance problems', output.getvalue())

    def new_opened_issue(self) -> Dict[str, str]:
        return {
            'id': 891887238,
            'project_id': 278964,
            'action_name': 'closed',
            'target_id': 68751013,
            'target_iid': 230687,
            'target_type': 'Issue',
            'author_id': 4312863,
            'target_title': 'Investigate performance problems',
            'created_at': '2020-09-03T06:48:54.160Z',
            'author': {'id': 4312863,
                       'name': 'Jo Bloggs',
                       'username': 'jo_bloggs',
                       'state': 'active',
                       'avatar_url': 'https://secure.gravatar.com/avatar/1234?s=80&d=identicon',
                       'web_url': 'https://gitlab.com/jo_bloggs'},
            'author_username': 'jo_bloggs'
        }
