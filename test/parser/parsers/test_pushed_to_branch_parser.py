from typing import Dict

from io import StringIO
from unittest import TestCase
from src.parser.parsers.pushed_to_branch_parser import PushedToBranchParser


class TestPushedToBranchParser(TestCase):

    def test_should_parse_push_to_branch(self):
        event = PushedToBranchParser().parse(self.push_to_branch())
        output = StringIO()

        event.summarize(output.write)
        self.assertEqual('Pushed 2 commit(s) to branch master, '
                         'latest commit Merge branch \'add-evidence-body\' into \'master\'', output.getvalue())

    def push_to_branch(self) -> Dict[str, str]:
        return {
            'id': 891763005,
            'project_id': 16683102,
            'action_name': 'pushed to',
            'target_id': None,
            'target_iid': None,
            'target_type': None,
            'author_id': 4312863,
            'target_title': None,
            'created_at': '2020-09-03T04:18:45.160Z',
            'author': {'id': 4312863,
                       'name': 'Jo Bloggs',
                       'username': 'jo_bloggs',
                       'state': 'active',
                       'avatar_url': 'https://secure.gravatar.com/avatar/12344?s=80&d=identicon',
                       'web_url': 'https://gitlab.com/jo_bloggs'},
            'push_data': {'commit_count': 2,
                          'action': 'pushed',
                          'ref_type': 'branch',
                          'commit_from': '28626bdcd540bae36f3a668a6b5226d98ad9ac19',
                          'commit_to': '8006101716752afac1be4fdfc89c4ec2fffc8f65',
                          'ref': 'master',
                          'commit_title': "Merge branch 'add-evidence-body' into 'master'",
                          'ref_count': None},
            'author_username': 'jo_bloggs'
        }
