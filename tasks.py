from invoke import Collection, run, task


@task
def unit(context, file=''):
    if file:
        run(f'python -m unittest discover --pattern {file} --start-directory test')
    else:
        run('python -m unittest discover --start-directory test')


test = Collection(unit)
ns = Collection(test=test)
