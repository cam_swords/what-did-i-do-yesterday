from typing import Any, Callable, List

from .event import Event


class Events:

    def __init__(self, events: List[Event]):
        self.events = events

    def summarize(self, printer: Callable[[str], Any]) -> None:
        for event in sorted(set(self.events)):
            event.summarize(printer)
