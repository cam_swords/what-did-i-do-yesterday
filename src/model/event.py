from functools import total_ordering
from typing import Callable


@total_ordering
class Event:

    def __init__(self, summary: str):
        self.summary = summary

    def summarize(self, printer: Callable[[str], None]) -> None:
        printer(self.summary)

    def __eq__(self, other) -> bool:
        return self.summary == other.summary

    def __lt__(self, other) -> bool:
        return self.summary < other.summary

    def __hash__(self) -> int:
        return hash(self.summary)
