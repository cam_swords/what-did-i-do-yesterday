

class Project:

    def __init__(self, name: str, web_url: str):
        self._name = name
        self._web_url = web_url

    def name(self) -> str:
        return self._name

    def url(self) -> str:
        return self._web_url

    def issue_url(self, issue_id: str) -> str:
        return f'{self._web_url}/-/issues/{issue_id}'

    def merge_request_url(self, mr_id: str) -> str:
        return f'{self._web_url}/-/merge_requests/{mr_id}'
