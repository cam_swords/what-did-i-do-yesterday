from typing import Optional, Dict, List

from src.model import Event, Events


class EventsParser:

    def __init__(self, *parsers):
        self._parsers = parsers

    def parse(self, raw_events: List[Dict[str, str]]) -> Events:
        events = [self.find_parser(raw_event) for raw_event in raw_events]
        return Events([event for event in events if event])

    def find_parser(self, raw_event: Dict[str, str]) -> Optional[Event]:
        for parser in self._parsers:
            if parser.understands(raw_event):
                return parser.parse(raw_event)

        print(f'Failed to parse event {raw_event}')
        return None
