from typing import Dict

from src.model import Event
from src.project import ProjectService


class DiscussionNoteParser:

    def __init__(self, project_service: ProjectService):
        self._project_service = project_service

    def understands(self, event: Dict[str, str]) -> bool:
        return 'action_name' in event and event['action_name'] == 'commented on'

    def parse(self, event: Dict[str, str]) -> Event:
        noteable_id = event['note']['noteable_iid']
        target_type = event['note']['noteable_type']
        title = event['target_title']

        if target_type == 'MergeRequest':
            project = self._project_service.load(event['project_id'])
            url = project.merge_request_url(noteable_id)
            return Event(f':eyesright: Reviewed MR <a href="{url}">{title}</a>')

        if target_type == 'Issue':
            project = self._project_service.load(event['project_id'])
            url = project.merge_request_url(noteable_id)
            return Event(f':speech_balloon: Commented on issue <a href="{url}">{title}</a>')

        return Event(f':speech_balloon: Commented on {target_type.capitalize()} #{noteable_id} {title}')
