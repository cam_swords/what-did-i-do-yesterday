from typing import Dict

from src.model import Event
from src.project import ProjectService


class JoinedProjectParser:

    def __init__(self, project_service: ProjectService):
        self._project_service = project_service

    def understands(self, event: Dict[str, str]) -> bool:
        return 'action_name' in event and event['action_name'] == 'joined' and (
                    'target_iid' not in event or not event['target_iid'])

    def parse(self, event: Dict[str, str]) -> Event:
        project = self._project_service.load(event['project_id'])
        return Event(f':heavy_plus_sign: Joined project <a href="{project.url()}">{project.name()}</a>')
