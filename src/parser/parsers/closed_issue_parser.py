from typing import Dict

from src.model import Event
from src.project import ProjectService


class ClosedIssueParser:

    def __init__(self, project_service: ProjectService):
        self._project_service = project_service

    def understands(self, event: Dict[str, str]) -> bool:
        return 'action_name' in event and event['action_name'] == 'closed' and event['target_type'] == 'Issue'

    def parse(self, event: Dict[str, str]) -> Event:
        issue_id = event['target_iid']
        title = event['target_title']

        project = self._project_service.load(event['project_id'])
        url = project.issue_url(issue_id)

        return Event(f':green: Closed issue <a href="{url}">{title}</a>')
