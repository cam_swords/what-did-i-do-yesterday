from typing import Dict

from src.model import Event


class PushedToBranchParser:

    def understands(self, event: Dict[str, str]) -> bool:
        return 'action_name' in event and event['action_name'] == 'pushed to'

    def parse(self, event: Dict[str, str]) -> Event:
        commits = event['push_data']['commit_count']
        ref_type = event['push_data']['ref_type']
        ref = event['push_data']['ref']
        latest_commit_title = event['push_data']['commit_title']

        return Event(f'Pushed {commits} commit(s) to {ref_type} {ref}, latest commit {latest_commit_title}')
