from typing import Dict

from src.model import Event
from src.project import ProjectService


class NewMergeRequestParser:

    def __init__(self, project_service: ProjectService):
        self._project_service = project_service

    def understands(self, event: Dict[str, str]) -> bool:
        return 'action_name' in event and event['action_name'] == 'opened' and event['target_type'] == 'MergeRequest'

    def parse(self, event: Dict[str, str]) -> Event:
        mr_id = event['target_iid']
        title = event['target_title']

        project = self._project_service.load(event['project_id'])
        url = project.merge_request_url(mr_id)

        return Event(f':ci_running: Created MR <a href="{url}">{title}</a>')
