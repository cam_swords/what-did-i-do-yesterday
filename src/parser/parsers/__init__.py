from .approved_merge_request_parser import ApprovedMergeRequestParser
from .closed_issue_parser import ClosedIssueParser
from .delete_branch_parser import DeleteBranchParser
from .discussion_note_parser import DiscussionNoteParser
from .joined_project_parser import JoinedProjectParser
from .new_merge_request_parser import NewMergeRequestParser
from .opened_issue_parser import OpenedIssueParser
from .pushed_to_branch_parser import PushedToBranchParser
from .pushed_to_new_branch_parser import PushedToNewBranchParser
from .closed_merge_request_parser import ClosedMergeRequestParser

__all__ = [
    'ApprovedMergeRequestParser',
    'ClosedMergeRequestParser',
    'ClosedIssueParser',
    'DeleteBranchParser',
    'DiscussionNoteParser',
    'NewMergeRequestParser',
    'OpenedIssueParser',
    'PushedToBranchParser',
    'PushedToNewBranchParser',
]
