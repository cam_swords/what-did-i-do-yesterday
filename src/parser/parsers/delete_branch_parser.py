from typing import Dict

from src.model import Event


class DeleteBranchParser:

    def understands(self, event: Dict[str, str]) -> bool:
        return 'action_name' in event and event['action_name'] == 'deleted'

    def parse(self, event: Dict[str, str]) -> Event:
        ref_type = event['push_data']['ref_type']
        ref = event['push_data']['ref']

        return Event(f'Deleted {ref_type} {ref}')
