

class HTMLPrinter:

    def __init__(self):
        self._summaries = []

    def print(self, summary: str):
        self._summaries.append(summary)

    def finalize(self):
        with open('yesterday.html', 'w') as html_file:
            html_file.write('<html><body>')

            for summary in self._summaries:
                html_file.write(f'<div>{summary}</div>')

            html_file.write('</body></html>')

        print('written yesterday.html')
