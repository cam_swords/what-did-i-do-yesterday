import requests
from datetime import date, timedelta
import json

from src.parser.parsers import ApprovedMergeRequestParser, ClosedMergeRequestParser, ClosedIssueParser, \
    DeleteBranchParser, DiscussionNoteParser, JoinedProjectParser, \
    NewMergeRequestParser, OpenedIssueParser, PushedToBranchParser, PushedToNewBranchParser
from .parser import EventsParser
from .project import ProjectService
from .html_printer import HTMLPrinter


class Activity:

    def __init__(self):
        project_service = ProjectService()
        self.events_parser = EventsParser(ApprovedMergeRequestParser(project_service),
                                          ClosedMergeRequestParser(project_service),
                                          ClosedIssueParser(project_service),
                                          DeleteBranchParser(),
                                          DiscussionNoteParser(project_service),
                                          NewMergeRequestParser(project_service),
                                          OpenedIssueParser(project_service),
                                          PushedToBranchParser(),
                                          PushedToNewBranchParser(),
                                          JoinedProjectParser(project_service))
        self.printer = HTMLPrinter()

    def summarize(self, date: date) -> None:
        with open('.gitlab-token') as token_file:
            token = token_file.read().strip()

        after = (date - timedelta(days=1)).isoformat()
        response = requests.get(f'https://gitlab.com/api/v4/events?after={after}', headers={'PRIVATE-TOKEN': token})

        events = self.events_parser.parse(json.loads(response.text))
        events.summarize(self.printer.print)
        self.printer.finalize()
