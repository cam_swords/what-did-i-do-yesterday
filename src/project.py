import requests
import json
from src.model import Project


class ProjectService:

    def load(self, project_id: str) -> Project:
        with open('.gitlab-token') as token_file:
            token = token_file.read().strip()

        response = requests.get(f'https://gitlab.com/api/v4/projects/{project_id}', headers={'PRIVATE-TOKEN': token})
        content = json.loads(response.text)

        return Project(content['name'], content['web_url'])


